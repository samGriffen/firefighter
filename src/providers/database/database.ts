import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';

@Injectable()
export class DatabaseProvider {
  
  items: any[] = [
    {
      name: "Truck 1",
      compartments: [
        {
          name: "Compartment 1",
          tools: [
            {
              name: "Halligan, T1C1",
              description: "Forcible entry tool used by firefighters and law enforcement",
              img: "tools/halligan.jpg",
              checked: false,
              missing: false
            },
            {
              name: "Halligan, T1C1",
              description: "Forcible entry tool used by firefighters and law enforcement",
              img: "tools/halligan.jpg",
              checked: false,
              missing: false
            },
            {
              name: "Halligan, T1C1",
              description: "Forcible entry tool used by firefighters and law enforcement",
              img: "tools/halligan.jpg",
              checked: false,
              missing: false
            }
          ]
        },
        {
          name: "Compartment 2",
          tools: [
              {
                name: "Halligan, T1C2",
                description: "Forcible entry tool used by firefighters and law enforcement",
                img: "tools/halligan.jpg",
                checked: false,
                missing: false
              },
              {
                name: "Halligan, T1C2",
                description: "Forcible entry tool used by firefighters and law enforcement",
                img: "tools/halligan.jpg",
                checked: false,
                missing: false
              },
              {
                name: "Halligan, T1C2",
                description: "Forcible entry tool used by firefighters and law enforcement",
                img: "tools/halligan.jpg",
                checked: false,
                missing: false
              }
          ]
        }
      ]
    }, 
    {
      name: "Truck 2",
      compartments: [
        {
          name: "Compartment 1",
          tools: [
            {
              name: "Halligan, T2C1",
              description: "Forcible entry tool used by firefighters and law enforcement",
              img: "tools/halligan.jpg",
              checked: false,
              missing: false
            },
            {
              name: "Halligan, T2C1",
              description: "Forcible entry tool used by firefighters and law enforcement",
              img: "tools/halligan.jpg",
              checked: false,
              missing: false
            },
            {
              name: "Halligan, T2C1",
              description: "Forcible entry tool used by firefighters and law enforcement",
              img: "tools/halligan.jpg",
              checked: false,
              missing: false
            }
          ]
        },
        {
          name: "Compartment 2",
          tools: [
              {
                name: "Halligan, T2C2",
                description: "Forcible entry tool used by firefighters and law enforcement",
                img: "tools/halligan.jpg",
                checked: false,
                missing: false
              },
              {
                name: "Halligan, T2C2",
                description: "Forcible entry tool used by firefighters and law enforcement",
                img: "tools/halligan.jpg",
                checked: false,
                missing: false
              },
              {
                name: "Halligan, T2C2",
                description: "Forcible entry tool used by firefighters and law enforcement",
                img: "tools/halligan.jpg",
                checked: false,
                missing: false
              }
          ]
        }
      ]
    },
  ];

  constructor(
    private storage: Storage,
    private sqlite: SQLite,
  ) {
    // for now just setting the mock data;
    this.storage.set("items", JSON.stringify(this.items));
  }

  getMasterList(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.storage.get("items").then((val) => {
        resolve(JSON.parse(val));
      });
    })
  }
}
