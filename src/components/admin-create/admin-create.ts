import { Component, OnInit, ViewChild } from '@angular/core';
import { DatabaseProvider } from '../../providers/database/database';
import { EditableAccordionComponent } from '../../components/editable-accordion/editable-accordion';

@Component({
  selector: 'admin-create',
  templateUrl: 'admin-create.html'
})
export class AdminCreateComponent implements OnInit {

  @ViewChild('editableAccordion') editableAccordion: EditableAccordionComponent
  items: any[] = [];
  currentTruck: string;

  constructor(
    private database_provider: DatabaseProvider
  ) {
  }

  ngOnInit(): void {
    // get a fresh list everytime they enter
    this.database_provider.getMasterList().then((data) => {
      this.items = data;
    });
  }

  currentTruckChange(truck: string): void {
    for(var i = 0; i < this.items.length; i++){
      if(this.items[i].name === truck){
        this.editableAccordion.make(this.items[i].compartments, "name", "asc")
        break;
      }
    }
  }

}
