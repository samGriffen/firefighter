import { Component, Output, EventEmitter } from '@angular/core';
import { NewItemPage } from '../../pages/new-item/new-item';
import { ModalController } from 'ionic-angular';

@Component({
  selector: 'editable-accordion',
  templateUrl: 'editable-accordion.html'
})
export class EditableAccordionComponent {

  @Output() cardClick = new EventEmitter<any>();
  accordionItems: any [] = [];
  active_item: string = "";
 
  constructor(
    public modalCtrl: ModalController
  ) {}

  make(data: any, key: string, order: string){
      let accordion: any[] = [];

      this.active_item = key; //will need this to diff. what information is showing in the view

      for(let i = 0; i < data.length; i++){
        let name: any = data[i].name;
        let position: number = this.getIndex(name, accordion);

        if(position < 0){
          accordion.push({name: name, children: [data[i]]});
        } else {
          accordion[position].children.push(data[i]);
        }
      }

      // // sort the children
      // for(let i = 0; i < accordion.length; i++){
      //   try {
      //     // if prismic doc
      //     accordion[i].children = orderBy(accordion[i].children, 'rawJSON[fighter_name]', "asc");
      //   } catch(e){}
      // }

      // sort the parent;
      // this.accordionItems = orderBy(accordion, "name", order);
      this.accordionItems = accordion
  }

  private getIndex(name: string, items: any[]): number {

    for(var i = 0; i < items.length; i++){
      if(typeof name === "string"){
        if(items[i].name.toLowerCase() === name.toLowerCase()){
          return i;
        }
      } else {
        if(items[i].name === name){
          return i;
        }
      }
    }

    return -1;
  }

  toggleSection(i: number): void {
    this.accordionItems[i].open = !this.accordionItems[i].open;
  }
 
  toggleItem(i: number, j: number): void {
    this.accordionItems[i].children[j].open = !this.accordionItems[i].children[j].open;
  }

  tracker(index: number, item: any): number {
    return index;
  }

  addItem() {
    let modal = this.modalCtrl.create(NewItemPage);
    modal.present();
  }

}
