import { Component, ViewChild } from '@angular/core';
import { NavController } from 'ionic-angular';
import { AccordionComponent } from '../../components/accordion/accordion';
import { DatabaseProvider } from '../../providers/database/database';

@Component({
  selector: 'page-checklist',
  templateUrl: 'checklist.html'
})
export class ChecklistPage {

  items: any[] = [];
  currentTruck: string = "Truck 1";

  status: any = {
    total: 0,
    checked: 0,
    missing: 0
  }

  @ViewChild('accordion') accordion: AccordionComponent

  constructor(
    private database_provider: DatabaseProvider
  ) {}

  ionViewDidEnter(): void {
    // get a fresh list everytime they enter
    this.database_provider.getMasterList().then((data) => {
      this.items = data;
      this.accordion.make(this.items[0].compartments, "name", "asc");
      this.updateStatus();
    });
  }

  currentTruckChange(truck: string): void {
    let truckIndex: number = this.getTruckIndex(truck);
    this.accordion.make(this.items[truckIndex].compartments, "name", "asc");
    this.updateStatus();
  }

  updateStatus(): void {
    let missing: number = 0;
    let checked: number = 0;
    let total: number = 0;
    let truckIndex: number = this.getTruckIndex(this.currentTruck);

    for(let i = 0; i < this.items[truckIndex].compartments.length; i++){
      for(let j = 0; j < this.items[truckIndex].compartments[i].tools.length; j++){
        let item: any = this.items[truckIndex].compartments[i].tools[j];
        total++;
        if(item.missing) missing++;
        if(item.checked) checked++;
      }
    }

    this.status.total = total;
    this.status.checked = checked;
    this.status.missing = missing;
  }

  getTruckIndex(truck: string): number {
    for(let i = 0; i < this.items.length; i++){
      if(this.items[i].name === truck){
        return i;
      }
    }
  }

  tracker(index: number, item: any): number {
    return index;
  }

  submit(){
    console.log(this.items);
  }

}
