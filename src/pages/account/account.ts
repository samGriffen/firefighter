import { Component } from '@angular/core';
import { ToastController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { DatabaseProvider } from '../../providers/database/database';

@Component({
  selector: 'page-account',
  templateUrl: 'account.html'
})
export class AccountPage {

  username: string;
  password: string;
  segment: string = "create";

  loggedIn: boolean = false;
  users: any = [
    {
      username: "Test",
      password: "admin"
    }
  ]

  constructor(
    public toastCtrl: ToastController,
    private database: DatabaseProvider,
    private storage: Storage
  ) {

  }

  ionViewDidLoad() {
    this.storage.get('currentUser').then((val) => {
      let user: any = val;
      if(user){
        this.loggedIn = true;
        this.username = user.username;
      }
    });
  }
  
  submit(): void {
    let foundUser: boolean = false;

    for(var i = 0; i < this.users.length; i++){
      let user: any = this.users[i];
      if(user.username.toLowerCase() === this.username.toLowerCase()){
        foundUser = true;
        if(user.password === this.password){
          this.loggedIn = true;
          this.storage.set("currentUser", user);
        } else {
          this.presentToast("Incorrect Password");
        }
        break;
      }
    }

    if(!foundUser){
      this.presentToast("Incorrect Username");
    }
  }

  signout(): void {
    this.username = null;
    this.password = null;
    this.loggedIn = false;
    this.storage.remove("currentUser");
  }

  presentToast(msg: string): void  {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: "middle"
    });
    toast.present();
  }

}
