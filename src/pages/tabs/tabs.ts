import { Component } from '@angular/core';

import { ChecklistPage } from '../checklist/checklist';
import { AccountPage } from '../account/account';
import { HomePage } from '../home/home';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = HomePage;
  tab2Root = ChecklistPage;
  tab3Root = AccountPage;

  constructor() {

  }
}
