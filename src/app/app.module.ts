import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { ChecklistPage } from '../pages/checklist/checklist';
import { AccountPage } from '../pages/account/account';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';
import { NewItemPage } from '../pages/new-item/new-item';

import { AccordionComponent } from '../components/accordion/accordion';
import { AdminCreateComponent } from '../components/admin-create/admin-create';
import { EditableAccordionComponent } from '../components/editable-accordion/editable-accordion';
import { ProgressBarComponent } from '../components/progress-bar/progress-bar';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { IonicStorageModule } from '@ionic/storage';

import { File } from '@ionic-native/file';
import { Transfer } from '@ionic-native/transfer';
import { FilePath } from '@ionic-native/file-path';
import { Camera } from '@ionic-native/camera';
import { SQLite } from '@ionic-native/sqlite';

import { DatabaseProvider } from '../providers/database/database';

@NgModule({
  declarations: [
    MyApp,
    ChecklistPage,
    AccountPage,
    HomePage,
    TabsPage,
    NewItemPage,
    AccordionComponent,
    AdminCreateComponent,
    EditableAccordionComponent,
    ProgressBarComponent
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    ChecklistPage,
    AccountPage,
    HomePage,
    TabsPage,
    NewItemPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    SQLite,
    File,
    Transfer,
    Camera,
    FilePath,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    DatabaseProvider
  ]
})
export class AppModule {}
